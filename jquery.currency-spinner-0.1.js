(function() {
	
	$.fn.currencySpinner = function(options, arg) {
	    
		var optionsKey = 'currency-spinner-options';
		var opts = null;

		$.fn.currencySpinner.getRawValue = function (curValue, elem) {
		    opts = $.fn.currencySpinner.getOptsData(elem);

		    if (!opts) return 0.0;
		    if (curValue == '') return opts.min;

		    var tmpValue = curValue.replace(opts.prefix, '').replace(',', '').replace(opts.suffix, '');

		    if (isNaN(parseFloat(tmpValue))) return 0.0;
		    return parseFloat(tmpValue);
		};

		$.fn.currencySpinner.getFormattedValue = function (elem) {
		    var curValue = $.fn.currencySpinner.getRawValue(elem.val(), elem);
		    return opts.prefix + $.fn.currencySpinner.addCommas(curValue.toFixed(2)) + opts.suffix;
		};

		$.fn.currencySpinner.addCommas = function (curValue) {
		    return curValue.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");  //http://stackoverflow.com/a/1990590
		};

	    $.fn.currencySpinner.getOptsData = function (elem) {
	        return elem.data(optionsKey);
	    };

	    if (options && typeof(options) == 'string') {
	        if (options == 'value') {
	        	if (typeof(arg) == 'number') {
	        		$(this).each(function() { $(this).val(arg).blur(); });
	        		return;
	        	}

	            return $.fn.currencySpinner.getRawValue($(this).val(), $(this));
	        }

	        if (options == 'formattedValue') {
	            return $.fn.currencySpinner.getFormattedValue($(this));
	        }

	        return;
	    }

	    $(this).each(function () {

	        var elem = $(this);
	        var previousValue = 0;
			var targetControl = elem.attr('id');
			var upArrowId = 'up-' + targetControl;
			var downArrowId = 'dn-' + targetControl;
			var arrowSelector = '#' + upArrowId + ', #' + downArrowId;

			elem.data(optionsKey, $.extend({}, $.fn.currencySpinner.defaultOptions, options));

			elem.off('keydown').keydown(function (e) {

                if (e.keyCode != 38 && e.keyCode != 40) return  // bail if anything BUT up/down

				var newValue = getRawValue(elem.val());

				if (e.keyCode == 38) {
					newValue += opts.step;
				}
				else if (e.keyCode == 40) {
					newValue -= opts.step;
				}

				if (typeof(opts.validation) == 'function') {
				    var valid = opts.validation(newValue);
					if (!valid) return;
				}

				if (newValue > opts.max || newValue < opts.min) return;

				elem.val(newValue);
			});

			$(document)
				.off('blur', arrowSelector)
                .on('blur', arrowSelector, function () { elem.blur(); })  // show formatted value on icon blur
                .off('click', arrowSelector)
                .on('click', arrowSelector, function (e) {
				    var direction = $(this).attr('data-dir');
					elem.trigger(jQuery.Event('keydown', { keyCode: (direction == 'up') ? 38 : 40 }));
				});

			elem.off('focus').focus(function (e) {
			    previousValue = getRawValue(elem.val());
			    elem.val(previousValue).select();
			});

			elem.mouseup(function (e) { e.preventDefault(); });  // select() hack for chrome and safari  http://stackoverflow.com/questions/1269722

			elem.off('blur').blur(function (e) {
			    var formattedVal = $.fn.currencySpinner.getFormattedValue(elem);
			    var rawValue = getRawValue(formattedVal);

			    if (rawValue == previousValue) {
			        elem.val(formattedVal);
			        return;
			    }

			    if (typeof (opts.validation) == 'function') {
			        var valid = opts.validation(rawValue);
			        if (!valid) {
			            elem.val(previousValue);
			            return false;
			        }
			    }

			    elem.val(formattedVal);
			});

			var appendSpinners = function(elem) {
			    elem.wrap("<div class='currency-spinner-wrapper' />")
					.after("<a id='" + downArrowId + "' class='dn' tabindex='-1' data-dir='down'><div class='arrow-down'></div></a>")
					.after("<a id='" + upArrowId + "' class='up' tabindex='-1' data-dir='up'><div class='arrow-up'></div></a>");
			};

			var getRawValue = function (curValue) {
			    opts = $.fn.currencySpinner.getOptsData(elem);  // <- important to all calling functions
			    return $.fn.currencySpinner.getRawValue(curValue, elem);
			};
            
            if ($.fn.currencySpinner.getOptsData(elem).showArrowIcons) {
				appendSpinners(elem);  // add the up/dn icons
            }

			elem.blur();  // blur to set formatted value on init

	    });

	};

	$.fn.currencySpinner.defaultOptions = {
	    min: 0,
	    max: 100000,
	    step: 5,
	    prefix: '$',
	    suffix: ' /hr',
	    validation: null,
	    showArrowIcons: true
	};
	
})(jQuery);